<?php


namespace Test\QingYa\Helper;

use QingYa\Helper\RedisUtils;

/**
 * Redis测试
 * @package Test\QingYa\Helper
 */
class RedisTest extends BaseTest
{

    /**
     * Redis获取锁、解锁测试
     */
    public function testLock()
    {
        // 如果实际项目中有.env环境变量中有 redis配置的话不需要这一步
        RedisUtils::connect('127.0.0.1', 6379);

        $lockKey = 'test';
        // 第一次获取锁 预期成功
        $result = RedisUtils::lock($lockKey, 5);
        self::assertSame(true, $result);
        // 第二次获取锁 预期失败
        $result = RedisUtils::lock($lockKey, 5);
        self::assertSame(false, $result);
        // 第三次sleep 5 秒后获取锁 预期成功
        sleep(5);
        $result = RedisUtils::lock($lockKey, 5);
        self::assertSame(true, $result);
        // 手动解锁
        RedisUtils::unlock($lockKey);
        // 第四次获取锁 预期成功
        $result = RedisUtils::lock($lockKey, 5);
        self::assertSame(true, $result);

    }
}