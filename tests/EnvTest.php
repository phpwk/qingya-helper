<?php


namespace Test\QingYa\Helper;

use QingYa\Helper\EnvUtils;

/**
 * Env测试
 * @package Test\QingYa\Helper
 */
class EnvTest extends BaseTest
{

    /**
     * 获取常量
     */
    public function testGet()
    {
        // 如果实际项目中有.env环境变量中有的话
        $result = EnvUtils::get('');
        self::assertSame(true, $result);


    }
}