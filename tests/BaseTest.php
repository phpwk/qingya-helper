<?php

namespace Test\QingYa\Helper;

use PHPUnit\Framework\TestCase;

/**
 * 测试基类
 * @package Test\QingYa\Helper
 */
abstract class BaseTest extends TestCase
{
    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        parent::setUp();
    }
}