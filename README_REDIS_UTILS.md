### 简介

Redis工具类

### 安装

```shell

composer require qingya/helper

```
### Redis配置 .env中配置
> [redis]  
  host      = 127.0.0.1  
  select    = 1  
  port      = 6379  
  password  = redispass  

针对TP框架也支持 application/config.php cache里面配置redis

### 用法


> 多个redis配置的锁是没有关系的，只锁定当前配置里面，针对TP，默认redis，自动判断，如果是tp自动走tp的缓存驱动，否则走原生connect
```php
use QingYa\Helper\RedisUtils;
// 实际是调用cache.redis_slave里面的配置进行的锁操作
$result = RedisUtils::lock('lockkey', 5, 'redis_e');
// 需要注意：下面和上面两个驱动配置不一样，实际是两个单独的锁，两个锁之间没有联系
$result = RedisUtils::lock('lockkey', 5, 'redis');
```

Redis锁（可以在高并发下保证操作的原子性）
```php
use QingYa\Helper\RedisUtils;

$result = RedisUtils::lock('lockkey', 5);
// 第一次调用获取到锁 返回true，5秒内多次访问返回false，超过5秒后会自动释放锁
var_dump($result);
```

Redis解锁(一般不需要解锁，过期后自动解锁，根据业务需要调用)
```php
use QingYa\Helper\RedisUtils;

RedisUtils::unlock('lockkey');
```

Redis原生操作
```php
use QingYa\Helper\RedisUtils;

$redis = RedisUtils::connect();
$redis->exists('key');
$redis->del('key');
```

