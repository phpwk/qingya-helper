### 简介

Env工具类

### 安装

```shell

composer require qingya/helper

```

### 用法


> 针对不同框架获取env配置不同，兼容多个，目前兼容:thinkPHP、yii2
```php
use QingYa\Helper\EnvUtils;
// 实际是调用,走框架env获取
$result = EnvUtils::get('key');
```
