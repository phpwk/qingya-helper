<?php
declare(strict_types=1);

namespace QingYa\Helper;

class EnvUtils
{
    /**
     * 获取环境变量值
     * @access public
     * @param string $name    环境变量名（支持二级 . 号分割）
     * @param string $default 默认值
     * @return bool|string|string[]|null
     */
    public static function get($name, $default = null)
    {
        //判断部分框架
        if (defined('THINK_VERSION')) {
            class_alias('think\\Env', 'EnvAlias');//tp5.0
        } elseif (defined('\think\App::VERSION')) {
            class_alias('think\\facade\\Env', 'EnvAlias');//>=tp5.1
        }
        if (class_exists('EnvAlias')) {
            return \EnvAlias::get($name, $default);
        }
        //使用原生getenv 获取
        $result = getenv($name);
        if ($result === false) {
            return $default;
        }
        $result = str_replace("\r", "", str_replace("\n", "", $result));
        if ('false' === $result) {
            $result = false;
        } elseif ('true' === $result) {
            $result = true;
        }
        return $result;
    }

}
