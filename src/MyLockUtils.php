<?php
declare(strict_types=1);

namespace QingYa\Helper;

use Exception;

/**
 * 自定义锁
 *
 * @package QingYa\Helper
 */
class MyLockUtils
{
    private static $_keyPre = 'myLock:';

    /**
     * 获取锁
     *
     * @param String $lockKey 锁标识
     * @param Int    $expire  锁过期时间
     * @param string $driver  redis驱动配置，默认留空通过env获取配置，部分项目可以选择redis/redis_slave,yii=yii项目默认走yii的缓存组件
     *                        如果可以尽量使用redis
     * @return Boolean
     */
    public static function lock($lockKey, $expire = 5, $driver = 'yii')
    {
        if ($driver != 'yii') {
            return RedisUtils::lock($lockKey, $expire, $driver);
        }
        try {
            $cache      = \Yii::$app->getCache();
            $lockKey    = self::$_keyPre . $lockKey;
            $expireTime = time() + $expire;
            if ($cache->exists($lockKey)) {
                return false;
            }
            $cache->set($lockKey, 1, $expire);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * 释放锁
     *
     * @param String $lockKey 锁标识
     * @param string $driver
     * @return Boolean
     */
    public static function unlock($lockKey, $driver = 'yii')
    {
        if ($driver != 'yii') {
            return RedisUtils::unlock($lockKey, $driver);
        }
        $lockKey = self::$_keyPre . $lockKey;
        try {
            $cache = \Yii::$app->getCache();
            return $cache->delete($lockKey);
        } catch (Exception $e) {
            return false;
        }
    }
}