<?php

namespace QingYa\Helper;


/**
 * 超级缓存辅助类
 */
class SuperCacheHelper
{
    protected $needCache = false;
    protected $data      = null;
    protected $ttl       = 0;

    /**
     * @return bool
     */
    public function isNeedCache(): bool
    {
        return $this->needCache;
    }


    /**
     * 设置是否需要缓存
     *
     * @param bool $needCache
     * @return SuperCacheHelper
     */
    public function setNeedCache(bool $needCache)
    {
        $this->needCache = $needCache;
        return $this;
    }

    /**
     * 缓存数据
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * 设置数据
     *
     * @param mixed $data
     * @return SuperCacheHelper
     */
    public function setData($data)
    {
        $this->data = $data;
        if (!empty($data)) {
            $this->needCache = true;
        }
        return $this;
    }

    /**
     * 获取缓存有效期
     *
     * @return int
     */
    public function getTtl(): int
    {
        return $this->ttl;
    }

    /**
     * 设置缓存有效期
     *
     * @param int $ttl
     * @return SuperCacheHelper
     */
    public function setTtl(int $ttl)
    {
        $this->ttl = $ttl;
        return $this;
    }


}
