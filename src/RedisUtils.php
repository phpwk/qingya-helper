<?php
declare(strict_types=1);

namespace QingYa\Helper;

use Exception;
use Redis;

/**
 * redis扩展类
 *
 * @package Dhcc\Helper
 */
class RedisUtils
{
    /**
     * @var $redis Redis
     */
    public static  $redis;
    private static $_keyPre = 'redisLock:';

    /**
     * 获取锁
     *
     * @param String $lockKey 锁标识
     * @param Int    $expire  锁过期时间
     * @param string $driver  redis驱动配置，默认留空通过env获取配置，部分项目可以选择redis/redis_slave
     * @return Boolean
     */
    public static function lock($lockKey, $expire = 5, $driver = 'redis')
    {
        $lockKey    = self::$_keyPre . $lockKey;
        $expireTime = time() + $expire;
        try {
            $redis  = $driver ? self::connectFromCacheDriver($driver) : self::connect();
            $isLock = $redis->set($lockKey, $expireTime, ['nx', 'ex' => $expire]);
            // 不能获取锁
            if (!$isLock) {
                // 判断锁是否过期
                $lockTimeCurrent = $redis->get($lockKey);
                // 锁已过期，删除锁，重新获取
                if (time() > $lockTimeCurrent) {
                    // 返回上次锁定到期时间
                    $lockTimePre = $redis->getSet($lockKey, $expireTime);
                    if ($lockTimePre == $lockTimeCurrent) {
                        $isLock = true;
                    }
                }
            }
            if ($isLock) {
                // 设置ttl，自动删除锁
                $redis->setex($lockKey, $expire, $expireTime);
            }
        } catch (Exception $e) {
            $isLock = false;
        }
        return (bool)$isLock;
    }

    /**
     * 释放锁
     *
     * @param String $lockKey 锁标识
     * @param string $driver
     * @return Boolean
     */
    public static function unlock($lockKey, $driver = 'redis')
    {
        $lockKey = self::$_keyPre . $lockKey;
        try {
            $redis = $driver ? self::connectFromCacheDriver($driver) : self::connect();
            return $redis->del($lockKey);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * 创建redis连接
     *
     * @param null $host
     * @param null $port
     * @param null $password
     * @param null $select
     * @param null $timeout
     * @return Redis
     */
    public static function connect($host = null, $port = null, $password = null, $select = null, $timeout = null)
    {
        if (!self::$redis) {
            $redis    = new Redis();
            $host     = $host ?? EnvUtils::get('redis.host', '127.0.0.1');
            $port     = $port ?? EnvUtils::get('redis.port', 6379);
            $password = $password ?? EnvUtils::get('redis.password');
            $select   = $select ?? EnvUtils::get('redis.select', 1);
            $timeout  = $timeout ?? EnvUtils::get('redis.timeout', 2);
            $redis->pconnect($host, (int)$port, (float)$timeout, 'persistent_id_' . $select);
            if ($password) {
                $redis->auth($password);
            }
            $redis->select((int)$select);
            self::$redis = $redis;
        }
        return self::$redis;
    }

    /**
     * 创建redis连接
     *
     * @param string $driver
     * @return Redis|object
     */
    public static function connectFromCacheDriver($driver = 'redis')
    {
        if (!class_exists('CacheAlias')) {
            //兼容部分框架:TP
            if (defined('THINK_VERSION')) {
                class_alias('think\\Cache', 'CacheAlias');//=tp5.0
            } elseif (defined('\think\App::VERSION')) {
                class_alias('think\\facade\\Cache', 'CacheAlias');//>=tp5.1
            } else {
                return self::connect();
            }
        }
        return \CacheAlias::store($driver)->handler();
    }


}