<?php
if (!function_exists('super_cache')) {

    /**
     * 只有Yii2使用自己的组件缓存，其他框架调用走的redisUtils 默认配置 select=2
     * 超级缓存（默认支持缓存自动预热）
     * @param string  $name        缓存key
     * @param Closure $valueFun    获取缓存数据的匿名函数或者闭包
     * @param integer $expire      缓存有效期
     * @param boolean $forceUpdate 是否强制更新缓存
     * @param boolean $useCache    是否使用缓存，针对没有配置redis等缓存的情况
     * @return bool|mixed|string|null
     * @throws Exception
     */
    function super_cache(string $name, Closure $valueFun, int $expire, $forceUpdate = false, $useCache = true)
    {
        //不使用缓存直接返回
        $self = new \QingYa\Helper\SuperCacheHelper();
        $valueFun($self);
        if(!$useCache){
            return $self->getData();
        }

        $nowTime = time();
        $isRaw   = false;
        //暂只支持Yii2
        if (class_exists("\\Yii")) {
            $cache = \Yii::$app->getCache();
        } else {
            $isRaw = true;//表示走的原生redis，需要手动处理set或get数据
            $cache = \QingYa\Helper\RedisUtils::connect();
            $cache->select(2);//可以走配置
        }
        if (!$cache) {
            throw new Exception('缓存组件加载失败');
        }
        $data = false;
        if (!$forceUpdate) {
            try {
                $data = $cache->get($name);
                if ($isRaw === true) {
                    $data = 0 === strpos($data, 'super_cache_serialize:') ? unserialize(substr($data, 22)) : $data;
                }
            } catch (Exception $e) {
            }
        }
        $needGetData = false;
        if (empty($data) || $forceUpdate) {
            // 缓存不存在或者强制更新缓存
            $needGetData = true;
        } else {
            // 缓存存在
            $expireTime = $data['__expiretime'] ?? 0;
            // 缓存剩余10秒时候预热缓存
            if ($expireTime > $nowTime && $expireTime - $nowTime <= 10) {
                // 优先使用 从redis锁
                if (\QingYa\Helper\RedisUtils::lock($name)) {
                    // 执行获取缓存
                    $needGetData = true;
                }
            }
        }
        $returnData = is_array($data) && array_key_exists('__super', $data) ? $data['__super'] : $data;
        if ($needGetData) {
            $self = new \QingYa\Helper\SuperCacheHelper();
            $valueFun($self);
            $needCache  = $self->isNeedCache();
            $returnData = $superData = $self->getData();
            $ttl        = $self->getTtl();
            $expire     = $ttl > 0 ? $ttl : $expire;
            $dataNew    = ['__super' => $superData, '__expiretime' => $nowTime + $expire, '__updatetime' => $nowTime, '__ttl' => $expire];
            if ($needCache) {
                if ($isRaw === true) {
                    $dataNew = is_scalar($dataNew) ? $dataNew : 'super_cache_serialize:' . serialize($dataNew);
                }
                $cache->set($name, $dataNew, $expire);
            }
        }
        return $returnData;
    }
}