# 常用工具包 qingya-helper

### 简介

php开发常用工具包，封装一些php常用助手类或函数

### 安装

```shell

composer require qingya/helper

```

### 目录

* [ENV工具类](README_ENV_UTILS.md)
* [Redis工具类](README_REDIS_UTILS.md)

### 更新日志

[Release Notes](UPGRADE.md)

### 帮助文档

### 许可

[MIT](LICENSE)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
